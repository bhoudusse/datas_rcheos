Liste **en construction** de ressources SIG téléchargeables à disposition des archéologues.[^1] 


**De manière générale, voir le site de l'IGN [Espace professionnel](http://professionnels.ign.fr/donnees) pour toutes les données gratuites, tant au format vectoriel qu'au format image**  

---

# Données IGN : limites administratives et fonds de carte

## Format vecteur (shapefile)

### [ADMIN_EXPRESS](http://professionnels.ign.fr/adminexpress#tab-2)

   Pour la France (Métropole et DOM), sur le site de l'IGN (espace profesionnel), le jeu de données qui offre les limites administratives depuis la commune jusqu'à la région
   
   ![ADMIN EXPRESS](http://professionnels.ign.fr/sites/default/files/image%20carousel/ADMIN_EXPRESS.jpg)

### [GEOFLA](http://professionnels.ign.fr/adminexpress#tab-3)  

   Accessible depuis la même page que ADMIN_EXPRES, GEOFLA donnes les mêmes informations mais avec des tracés moins précis (donc plus légérs), plus adaptés pour des cartes à grande échelle


## Format raster (image)

### Les cartes actuelles : sur l'[Espace Professionel de l'IGN](http://professionnels.ign.fr/donnees)  

Les cartes à télécharger gratuitement sont celles à petite échelle (donc pour des vues larges) comme :


* le SCAN 1000 

  ![SCAN 1000](http://professionnels.ign.fr/sites/default/files/image%20carousel/scan1000.png)


* le SCAN Express : attention **il ne correspond pas à la carte au 25 000e (=SCAN 25)**  

  <img src="http://professionnels.ign.fr/sites/default/files/image%20carousel/Im2-ScanExpress50-Classique_0.png" alt="ScanExpress" width="300" height="300">


### Les cartes anciennes : sur le site [Remonter le temps](https://remonterletemps.ign.fr/)


On peut télécharger des cartes anciennes :

* Les cartes d'Etat major

  Il s'agit en fait des minutes de la carte d'Etat major, non géoréférencées. 
  Elles sont accessibles à cette [adresse](https://remonterletemps.ign.fr/telecharger?x=2.117437&y=46.489054&z=6&layer=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD&demat=ETATMAJOR$GEOPORTAIL:DEMAT)


## Format WMS (image depuis un serveur)
1. Serveur du BRGM délivrant certaines données de l'IGN (format image) : **accessible depuis n'importe où**  

   Saisir l'adresse suivante dans le panneau **_Créer une nouvelle connexion WMS_** de Qgis  
   
   ![Qgis connexion WMS](https://docs.qgis.org/2.18/fr/_images/connection_wms.png)
   
   >```http://mapsref.brgm.fr/wxs/refcom-brgm/refign```

   Une fois l'adresse créée, cliquer sur **_Connexion_**, puis choisir la couche appropriée, vérifier le format d'image et la projection, puis cliquer sur **_Ajouter_**


---


# Plan cadastral informatisé (PCI)

Les différentes couches du plan cadastral informatisé (PCI) sont depuis 2017 disponibles au téléchargement pour toutes les communes où ce document est vectorisé.  

Les données brutes sont accessibles par le site [cadastre.gouv.fr](https://cadastre.data.gouv.fr/) et on peut les __télécharger facilement__ sur la page du [Cadastre Etalab](https://cadastre.data.gouv.fr/datasets/cadastre-etalab)  

Sur cette page, il faut descendre tout en bas pour accéder à la section **Aide au téléchargement** 

Il suffit alors de procéder dans l'ordre:  
1. Choisir de télécharger par département ou par commune
2. Saisir le nom du territoire (commune ou département)
3. Cliquer sur le format désiré (SHP pour du shapefile)
4. Sélectionner les entités que l'on souhaite télécharger (parcelles, bâtiments, mais aussi limites cadastrales de commune, section, feuille, lieu-dit...)
5. Cliquer sur le bouton __télécharger__ pour lancer le téléchargement

![Cadastre Etalab](screens/cad_etalab.png)


---


# Hydrographie

## Format vectoriel

L'IGN met à disposition le [*réseau hydrographique issu de la BD Topo*](http://professionnels.ign.fr/bdtopo-hydrographie#tab-3) au format shapefile.  
Les données comprennent les cours d’eau, les surfaces d’eau, les réservoirs et autres points d’eau. Ce thème comprend également les hydronymes.


---

# Relief (Modèle Numérique de Terrain)

## Format Raster

L'IGN met à disposition gratuitement une partie de la BD Alti, à une résolution de 75 ou 250 m.

Elle est téléchargeable à [cette adresse](http://professionnels.ign.fr/bdalti#tab-3)


## Format WMS

Fourni par le [CRAIG](https://www.craig.fr/), un fond de carte présentant une image du relief de la France métropolitaine, image du relief ombré et coloré (suivant les valeurs d'altitude et pente (en dégradé de gris) pour un rendu réaliste à petite échelle

Saisir l'adresse suivante dans le panneau **_Créer une nouvelle connexion WMS_** de Qgis  
> ````https://tiles.craig.fr/mnt/service````

Ajouter la couche dont le nom est ```relief```


---


# Carte géologique

## Format WMS
La carte géologique est disponible au format WMS.  
Plus d'infos sur la page dédiée aux **Géoservices** du [BRGM](http://infoterre.brgm.fr/page/geoservices-ogc)  

Saisir l'adresse suivante dans le panneau **_Créer une nouvelle connexion WMS_** de Qgis  
> ````http://geoservices.brgm.fr/geologie````

La carte géologique au 50 000e correspond à la couche qui porte le nom ```SCAN_D_GEOL50```.




[^1]: Version du 05/11/2018.

