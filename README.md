**Données (libres ou téléchargeables), outils, trucs et astuces pour les projets SIG des archéologues (avec QGIS)**

# Liste en construction de ressources à télécharger

[Liste](liste_data.md)

# Sources d'inspiration (carto, sémio graphique, data visualisation/dataviz)

Quelques sources à consulter sur [cette page](sources_inspi.md)

# Trucs et astuces QGIS

## Qgis 3.X

### Régler le point d'ancrage des étiquettes (avec lignes de rappel)

[tuto ici](trucs_etiquettage.md)