# Régler automatiquement la position du point d'ancrage des étiquettes dans Qgis 3.X

## Utile lorsque l'on utilise des étiquettes avec des lignes de rappel


Après avoir défini la position de l'étiquette (en utilisant le ``stockage auxiliaire``), dans le panneau d'étiquettage, il faut se rendre à l'onglet **Position** :  
puis dans le sous-menu **Donnée définie**, à la ligne **Alignement**, à côté de **horizontal**, cliquer la boîte de dialogue _**Définition de données**_ et choisir "éditer" :


![position](/screens/lbl_position_definie.png)


Il faut alors insérer l'expression suivante:  


```
CASE WHEN  "auxiliary_storage_labeling_positionx" < 'x($geometry)' THEN 'Right'
ELSE 'Left'
END
```

En clair, dès que la coordonnée X de l'étiquette est inférieure à celle du centroïde du polygone (donc à gauche du centroïde), le point d'ancrage de l'étiquette bascule à droite.

On passe alors automatiquement de cet aspect :  

![défaut](/screens/lbl_ancrage_defaut.png)



à celui-ci :  

![reglage](/screens/lbl_ancrage_regle.png)




