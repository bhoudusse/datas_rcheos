**Sélection non exhaustive de liens vers des exemples de cartographie**

# 1. Cartes anciennes  


1. Les cartes de **__Charles-Joseph Minard__**  
   1. Exemples visibles (et téléchargeables) sur [Gallica](https://gallica.bnf.fr/services/engine/search/sru?operation=searchRetrieve&version=1.2&startRecord=45&maximumRecords=15&page=4&query=%28%28dc.creator%20all%20%22Minard%2C%20Charles-Joseph%22%20or%20dc.contributor%20all%20%22Minard%2C%20Charles-Joseph%22%29%29)
   2. Ecrits et cartes dans la [bibliothèque numérique de l'Ecole nationale des Ponts et Chaussées](https://patrimoine.enpc.fr/collections/show/12?page=1&sort=asc)
   3. Une version interactive de la célèbre carte de la campagne de Russie [ici](http://goumprod.com/minard/)

2. A suivre
